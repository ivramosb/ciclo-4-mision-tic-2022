const express = require('express')
const app = express()
//Importo la conexion con mongoDB
const miconexion = require('./conexion')
//Colección de productos
const rutaProductos = require('./routes/productos')
//Importo el body parser
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
//Uso bodyParser
app.use('/api/productos', rutaProductos)

//Peticion get por defecto
app.get('/', (req, res) => {
    res.end("Servidor Backend OK!")
})

//Servidor 
app.listen(5000, function(){
    console.log("Servidor OK en puerto 5000 - http://localhost:5000")
})