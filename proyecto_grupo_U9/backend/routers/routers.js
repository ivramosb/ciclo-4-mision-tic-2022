const express = require('express');
const router = express.Router();

//Ruta para modulo productos
const controladorProductos = require('./router_productos');
router.use("/productos",controladorProductos);

module.exports = router