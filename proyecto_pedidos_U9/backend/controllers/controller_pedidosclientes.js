const express = require('express');
const router = express.Router();
const modeloPedidos = require('../models/model_pedidos');
const modeloClientes = require('../models/model_clientes');

router.get('/pedidosclientes', (req, res) => {
    modeloClientes.aggregate([
        {
            $lookup: {
                localField: "id",
                from: "pedidos",
                foreignField: "id_cliente",
                as: "pedidos_clientes",
            },
        },
        { $unwind: "$pedidos_clientes" }])
        .then((result)=>{res.send(result); console.log(result);})
        .catch((error)=>{res.send(error); console.log(error);});
});

router.get('/pedidosclientes/:id', (req, res) => {
    var dataClientes = [];
    modeloClientes.find({id:req.params.id}).then(data => {
        console.log("Datos del Cliente:");
        console.log(data);
        data.map((d, k) => {dataClientes.push(d.id);})
        modeloPedidos.find({id_cliente: { $in: dataClientes}})
        .then(data => {
            console.log("Pedidos del cliente:");
            console.log(data);
            res.send(data);
        })
        .catch((error)=>{res.send(error); console.log(error);});
    });
});




module.exports = router;